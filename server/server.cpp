#include <unistd.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <netdb.h>
#include <string.h>
#include <fcntl.h>
#include <pthread.h>
#include <dirent.h>
#include <map>
#include <string>

using namespace std;

struct Server_address {
    struct in_addr sin_addr;
    in_port_t sin_port;
};

struct Info_for_connect {
    Server_address server_address;
    sockaddr_in my_addr;

};

bool operator<(const Server_address& s1, const Server_address& s2) {
    return (s1.sin_addr.s_addr < s2.sin_addr.s_addr) || (s1.sin_port < s2.sin_port);
}

pthread_mutex_t table_mutex;
multimap<Server_address, string> files_table;
pthread_mutex_t connecting_servers_mutex;
map<Server_address, bool> connecting_servers;

ssize_t write_n_bytes(int descr, const char *buf, size_t n) {
    ssize_t n_was_written;
    size_t n_count = n;
    while (n_count > 0) {
        if ((n_was_written = write(descr, buf, n_count)) <= 0) {
            if (-1 == n_was_written) {
                if (EAGAIN == errno) {
                    continue;
                } else {
                    return -1;
                }
            }
        }
        n_count -= n_was_written;
        buf += n_was_written;
    }
    return n;
}

ssize_t read_n_bytes(int descr, char *buf, size_t n) {
    ssize_t n_was_readen;
    size_t n_count = n;
    while (n_count > 0) {
        if ((n_was_readen = read(descr, buf, n_count)) <= 0) {
            if (-1 == n_was_readen) {
                if (EAGAIN == errno) {
                    continue;
                } else {
                    return -1;
                }
            } else if (0 == n_was_readen) {
                break;
            }
        }
        n_count -= n_was_readen;
        buf += n_was_readen;
    }
    return n - n_count;
}

void reverse_str(char* str) {
    int i, j;
    char c;
    for (i = 0, j = strlen(str)-1; i<j; i++, j--) {
        c = str[i];
        str[i] = str[j];
        str[j] = c;
    }
}

void int_to_str(int n, char* str) {
    int i, sign;
    if ((sign = n) < 0) {
        n = -n;
    }
    i = 0;
    do {
        str[i++] = (n%10) + '0';
    } while ((n/=10) > 0);
    if (sign < 0) {
        str[i++] = '-';
    }
    str[i] = '\0';
    reverse_str(str);

}

int connect_to_listen_socket(struct Server_address server_address) {
	int sockfd;
    struct sockaddr_in serv_addr;
    if (-1 == (sockfd = socket(AF_INET, SOCK_STREAM, 0))) {
        return -1;
    }
    bzero(&serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = server_address.sin_port;
    serv_addr.sin_addr.s_addr = server_address.sin_addr.s_addr;
    if (-1 == connect(sockfd, (struct sockaddr*)&serv_addr, sizeof(serv_addr))) {
    	close(sockfd);
        return -1;
    }
    return sockfd;
}

void GET_command(int sockfd, char* buf) {
	char* pch = strtok(buf, " \r\n");
	if (NULL == pch) return;
    int file_descr;
    if (-1 == (file_descr = open(pch, O_RDONLY))) {
        perror(pch);
        return;
    }
    char send_arr[BUFSIZ];
    ssize_t n;
    while ((n = read_n_bytes(file_descr, send_arr, BUFSIZ)) > 0) {
        if (-1 == write_n_bytes(sockfd, send_arr, n)) {
            perror("write_n_bytes");
            if (-1 == close(sockfd)) {
            	perror("close");
            	pthread_exit(NULL);
            }
            pthread_exit(NULL);
        }
    }
    if (-1 == n) {
        perror("read_n_bytes");    
    }
    if (-1 == close(file_descr)) {
        perror("close");
    }
}

void GET_SIZE_command(int sockfd, char* buf) {
	char* pch = strtok(buf, " \r\n");
	if (NULL == pch) return;
    struct stat st;
    if (-1 == lstat(pch, &st)) {
    	perror(pch);
    	return;
    }
    char send_arr[BUFSIZ];
    strncpy(send_arr, "FILE_SIZE ", 10);
    off_t file_size = st.st_size;
    int_to_str(file_size, send_arr+10);
    size_t len = strlen(send_arr);
    send_arr[len] = '\r';
    send_arr[len+1] = '\n';
    if (-1 == write_n_bytes(sockfd, send_arr, len+2)) {
        perror("write_n_bytes");
        if (-1 == close(sockfd)) {
    		perror("close");
    		return;
    	}
    }
}

void SERVER_command(int sockfd, char* recv_arr) {
	char* pch;
	pch = strtok(recv_arr, " \r\n");
	if (NULL == pch) return;
    Server_address serv_and_addr;
    if (inet_pton(AF_INET, pch, &(serv_and_addr.sin_addr)) <= 0) {
        if (0 != errno) {
            perror("inet_pton");
            if (-1 == close(sockfd)) {
            	perror("close");
            	pthread_exit(NULL);
            }
            pthread_exit(NULL);
        } else {
            fprintf(stderr, "Bad format ip\n");
            return;
        }
    }
    pch = strtok(NULL, " \r\n");
    if (NULL == pch) return;
    char *ptr = NULL;
	serv_and_addr.sin_port = strtol(pch, &ptr, 10);
	if (ptr == pch || *ptr != 0) {
		fprintf(stderr, "Bad format port\n");
		return;
	}


	pthread_mutex_lock(&connecting_servers_mutex);
	if (connecting_servers.find(serv_and_addr) != connecting_servers.end()) {
		fprintf(stderr, "Server is already connected\n");
		pthread_mutex_unlock(&connecting_servers_mutex);	
		return;
	}
	connecting_servers.insert(std::pair<Server_address, bool>(serv_and_addr, 1));
    pthread_mutex_unlock(&connecting_servers_mutex);	
	

	while (NULL != (pch = strtok(NULL, " \r\n"))) {
		pthread_mutex_lock(&table_mutex);
	    auto it = files_table.find(serv_and_addr);
	    if (it == files_table.end() || it->second != string(pch)) {
	        files_table.insert(std::pair<Server_address, string>(serv_and_addr, string(pch)));
	    }
	    pthread_mutex_unlock(&table_mutex);	
	}

    char port_name[6];
    char ip_name[16];
    size_t send_len = 7;
    int count_address = 0;
    char count_address_str[10];
    pthread_mutex_lock(&connecting_servers_mutex);
    for (auto &connecting_server : connecting_servers) {
        if (connecting_server.first.sin_port != serv_and_addr.sin_port ||
                connecting_server.first.sin_addr.s_addr != serv_and_addr.sin_addr.s_addr) {
        	int fd;
        	Server_address temp;	
        	temp.sin_addr = connecting_server.first.sin_addr;
        	temp.sin_port = htons(connecting_server.first.sin_port);
        	if (-1 == (fd = connect_to_listen_socket(temp))) {
        		continue;
        	}
        	close(fd);
            inet_ntop(AF_INET, &(connecting_server.first.sin_addr), ip_name, INET_ADDRSTRLEN);
            int_to_str(connecting_server.first.sin_port, port_name);
            send_len += (strlen(ip_name) + 1);
            send_len += (strlen(port_name) + 1);
            ++count_address;
        }
    }
    errno = 0;
    pthread_mutex_unlock(&connecting_servers_mutex);

    int_to_str(count_address, count_address_str);
    send_len += (strlen(count_address_str) + 1);

    DIR* p_dir;
    struct dirent* p_dirent;
    p_dir = opendir("./");
    if (NULL == p_dir) {
        perror("opendir");
        exit(1);
    }

    while (NULL != (p_dirent = readdir(p_dir))) {
        if (strncmp(p_dirent->d_name, ".\0", 2) && strncmp(p_dirent->d_name, "..\0", 3) && strncmp(p_dirent->d_name, "server\0", 7)) {
            send_len += (strlen(p_dirent->d_name)+1);
        }
    }
    if (errno != 0) {
        perror("readdir");
        exit(1);
    }

    rewinddir(p_dir);
    char send_arr[send_len+2];
    size_t ind = 0;
    strncpy(send_arr, "NETWORK ", 8);
    strncpy(send_arr + 8, count_address_str, strlen(count_address_str));
    ind += (8 + strlen(count_address_str));

    pthread_mutex_lock(&connecting_servers_mutex);
    for (auto &connecting_server : connecting_servers) {
        if (connecting_server.first.sin_port != serv_and_addr.sin_port ||
                connecting_server.first.sin_addr.s_addr != serv_and_addr.sin_addr.s_addr) {
        	int fd;
        	Server_address temp;
        	temp.sin_addr = connecting_server.first.sin_addr;
        	temp.sin_port = htons(connecting_server.first.sin_port);
        	if (-1 == (fd = connect_to_listen_socket(temp))) {
        		continue;
        	}
        	close(fd);
            inet_ntop(AF_INET, &(connecting_server.first.sin_addr), ip_name, INET_ADDRSTRLEN);
            int_to_str(connecting_server.first.sin_port, port_name);
            send_arr[ind] = ' ';
            ++ind;
            strncpy(send_arr+ind, ip_name, strlen(ip_name));
            ind += strlen(ip_name);
            send_arr[ind] = ' ';
            ++ind;
            strncpy(send_arr+ind, port_name, strlen(port_name));
            ind += strlen(port_name);
        }
    }
    pthread_mutex_unlock(&connecting_servers_mutex);
    errno = 0;

    while (NULL != (p_dirent = readdir(p_dir))) {
        if (strncmp(p_dirent->d_name, ".\0", 2) && strncmp(p_dirent->d_name, "..\0", 3) && strncmp(p_dirent->d_name, "server\0", 7)) {
            size_t file_name_len = strlen(p_dirent->d_name);
            send_arr[ind] = ' ';
            ++ind;
            strncpy(send_arr+ind, p_dirent->d_name, file_name_len);
            ind += (file_name_len);
        }
    }
    strncpy(send_arr+ind, "\r\n", 2);
    if (-1 == write_n_bytes(sockfd, send_arr, ind+2)) {
        perror("write_n_bytes");
        pthread_mutex_lock(&table_mutex);
		files_table.erase(serv_and_addr);
		pthread_mutex_unlock(&table_mutex);

		pthread_mutex_lock(&connecting_servers_mutex);
	    connecting_servers.erase(serv_and_addr);
	    pthread_mutex_unlock(&connecting_servers_mutex);

        if (-1 == close(sockfd)) {
        	perror("close");
        	pthread_exit(NULL);
        }
        pthread_exit(NULL);
    }

    char ch;
    read(sockfd, &ch, 1);

    pthread_mutex_lock(&table_mutex);
	files_table.erase(serv_and_addr);
	pthread_mutex_unlock(&table_mutex);

	pthread_mutex_lock(&connecting_servers_mutex);
    connecting_servers.erase(serv_and_addr);
    pthread_mutex_unlock(&connecting_servers_mutex);

}

void SEARCH_command(int sockfd, char* buf) {
	char* pch = strtok(buf, " \r\n");
	if (NULL == pch) return;
    Server_address need_server;
    pthread_mutex_lock(&table_mutex);
    auto it = files_table.begin();
    for (; it != files_table.end(); ++it) {
        if (!strncmp(pch, it->second.c_str(), strlen(it->second.c_str())+1)) {
            need_server.sin_addr.s_addr = it->first.sin_addr.s_addr;
            need_server.sin_port = htons(it->first.sin_port);
            break;
        }
    }
    if (files_table.end() == it) {
        pthread_mutex_unlock(&table_mutex);
        if (-1 == write_n_bytes(sockfd, "INFO no file\r\n", 14)) {
            perror("write_n_bytes");
            if (-1 == close(sockfd)) {
            	perror("close");
            	pthread_exit(NULL);
            }
            pthread_exit(NULL);
        }
        return;
    }
    pthread_mutex_unlock(&table_mutex);
    int test_sockfd;
    if (-1 == (test_sockfd = connect_to_listen_socket(need_server))) {
    	pthread_mutex_lock(&table_mutex);
	    files_table.erase(need_server);
	    pthread_mutex_unlock(&table_mutex);

    	pthread_mutex_lock(&connecting_servers_mutex);
	    connecting_servers.erase(need_server);
	    pthread_mutex_unlock(&connecting_servers_mutex);

    	if (-1 == write_n_bytes(sockfd, "INFO no file\r\n", 14)) {
            perror("write_n_bytes");
            if (-1 == close(sockfd)) {
            	perror("close");
            	pthread_exit(NULL);
            }
            pthread_exit(NULL);
        }
        return;	
    }
    close(test_sockfd);

    need_server.sin_port = ntohs(need_server.sin_port);
    char ip_name[16];
    if (NULL == inet_ntop(AF_INET, &(need_server.sin_addr), ip_name, 16)) {
        perror("inet_ntop");
        if (-1 == close(sockfd)) {
        	perror("close");
        	pthread_exit(NULL);
        }
        pthread_exit(NULL);
    }

    char port_name[6];
    int_to_str(need_server.sin_port, port_name);

    char send_arr[BUFSIZ];
    size_t ind = 5;
    strncpy(send_arr, "INFO ", 5);
    strncpy(send_arr+ind, ip_name, strlen(ip_name));
    ind += strlen(ip_name);
    send_arr[ind] = ' ';
    ++ind;
    strncpy(send_arr+ind, port_name, strlen(port_name));
    ind += strlen(port_name);
    strncpy(send_arr+ind, "\r\n", 2);
    ind += 2;
    if (-1 == write_n_bytes(sockfd, send_arr, ind)) {
        perror("write_n_bytes");
        if (-1 == close(sockfd)) {
        	perror("close");
        	pthread_exit(NULL);
        }
        pthread_exit(NULL);
    }
}

void* do_work(void* args) {
    int sockfd = *((int*)args);
    free(args);
    int err_code;
    if (0 != (err_code = pthread_detach(pthread_self()))) {
        errno = err_code;
        pthread_exit(NULL);
    }
    char buf[BUFSIZ];
    off_t n;
    bool isFullCommand = false;
    char* recv_arr = NULL;

    size_t size = 0;
    while (1) {
        if ((n = recv(sockfd, buf, BUFSIZ, 0)) <= 0) {
            free(recv_arr);
            if (-1 == close(sockfd)) {
                perror("close socket");
                pthread_exit(NULL);
            }
            pthread_exit(NULL);
        }
        int i;
        for (i = 1; i < n; ++i) {
            if ('\r' == buf[i-1] && '\n' == buf[i]) {
                isFullCommand = true;
                break;
            }
        }
        if (!isFullCommand) {
            if (NULL == (recv_arr = (char*)realloc(recv_arr, n+size))) {
            	perror("realloc");
            	free(recv_arr);
            	if (-1 == close(sockfd)) {
	                perror("close socket");
	                pthread_exit(NULL);
	            }
            	pthread_exit(NULL);
            }
            strncpy(recv_arr+size, buf, n);
            size+=n;
            continue;
        } else {
            if (NULL == (recv_arr = (char*)realloc(recv_arr, i+1+size))) {
            	perror("realloc");
            	free(recv_arr);
            	if (-1 == close(sockfd)) {
	                perror("close socket");
	                pthread_exit(NULL);
	            }
            	pthread_exit(NULL);
            }
            strncpy(recv_arr+size, buf, i+1);
            size+=(i+1);
            if (!strncmp(recv_arr, "SEARCH ", 7)) {
                SEARCH_command(sockfd, recv_arr+7);
            } else if (!strncmp(recv_arr, "GET_SIZE ", 9)) {
                GET_SIZE_command(sockfd, recv_arr+9);
            } else if (!strncmp(recv_arr, "GET ", 4)) {
                GET_command(sockfd, recv_arr+4);
            } else if (!strncmp(recv_arr, "SERVER ", 7)) {
                SERVER_command(sockfd, recv_arr+7);
            }
            free(recv_arr);
            recv_arr = NULL;
            size = 0;
            if (i != n-1) {
            	if (NULL == (recv_arr = (char*)realloc(recv_arr, n-(i+1)))) {
            		perror("realloc");
            		if (-1 == close(sockfd)) {
		                perror("close socket");
		                pthread_exit(NULL);
		            }
	            	pthread_exit(NULL);
            	}
            	strncpy(recv_arr, buf+i+1, n-(i+1));
            	size+=(n-(i+1));
            }
        }
    }
}

void* connect_with_server(void* args) {
    struct Info_for_connect info_for_connect = *((struct Info_for_connect*)args);
    free(args);

    Server_address conn_server;
    conn_server.sin_addr.s_addr = info_for_connect.server_address.sin_addr.s_addr;
    conn_server.sin_port = ntohs(info_for_connect.server_address.sin_port);
    pthread_mutex_lock(&connecting_servers_mutex);
    auto it_con_serv = connecting_servers.find(conn_server);
    if (it_con_serv == connecting_servers.end()) {
        connecting_servers.insert(std::pair<Server_address, bool>(conn_server, 1));
    } else {
        if (!it_con_serv->second) {
            connecting_servers[conn_server] = 1;
        } else {
            pthread_mutex_unlock(&connecting_servers_mutex);
            pthread_exit(NULL);
        }
    }
    pthread_mutex_unlock(&connecting_servers_mutex);

    int another_sockfd = connect_to_listen_socket(info_for_connect.server_address);
    if (-1 == another_sockfd) {
    	perror("Cannot connect to listen socket");
    	pthread_exit(NULL);
    }

    DIR* p_dir;
    struct dirent* p_dirent;
    p_dir = opendir("./");
    if (NULL == p_dir) {
        perror("opendir");
        exit(1);
    }

    char str[29];
    strncpy(str, "SERVER ", 7);
    inet_ntop(AF_INET, &(info_for_connect.my_addr.sin_addr), str+7, INET_ADDRSTRLEN);
    size_t info_len = strlen(str);
    str[info_len] = ' ';
    int_to_str(htons(info_for_connect.my_addr.sin_port), str+info_len+1);
    info_len = strlen(str);

    size_t names_len = 0;
    while (NULL != (p_dirent = readdir(p_dir))) {
        if (strncmp(p_dirent->d_name, ".\0", 2) && strncmp(p_dirent->d_name, "..\0", 3) && strncmp(p_dirent->d_name, "server\0", 7)) {
            names_len += (strlen(p_dirent->d_name)+1);
        }
    }
    if (errno != 0) {
        perror("readdir");
        exit(1);
    }

    rewinddir(p_dir);
    char send_arr[names_len+info_len+2];
    size_t ind = 0;
    strncpy(send_arr, str, (ind+=info_len));
    while (NULL != (p_dirent = readdir(p_dir))) {
        if (strncmp(p_dirent->d_name, ".\0", 2) && strncmp(p_dirent->d_name, "..\0", 3) && strncmp(p_dirent->d_name, "server\0", 7)) {
            size_t file_name_len = strlen(p_dirent->d_name);
            send_arr[ind] = ' ';
            strncpy(send_arr+ind+1, p_dirent->d_name, file_name_len);
            ind += (file_name_len+1);
        }
    }
    strncpy(send_arr+ind, "\r\n", 2);
    if (-1 == write_n_bytes(another_sockfd, send_arr, names_len+info_len+2)) {
        perror("write_n_bytes");
        if (-1 == close(another_sockfd)) {
        	perror("close");
        }
        pthread_exit(NULL);
    }

    if (errno != 0) {
        perror("readdir");
        exit(1);
    }
    if (-1 == closedir(p_dir)) {
        perror("closedir");
        exit(1);
    }

    char buf[BUFSIZ];
    size_t n;
    char* recv_arr = NULL;
    size_t size = 0;
    bool isFullCommand = false;
    while (1) {
        if ((n = recv(another_sockfd, buf, BUFSIZ, 0)) <= 0) {
            perror("recv");
            if (-1 == close(another_sockfd)) {
            	perror("close");
            }
            pthread_exit(NULL);
        }
        int i;
        for (i = 1; i < n; ++i) {
            if ('\r' == buf[i-1] && '\n' == buf[i]) {
                isFullCommand = true;
                break;
            }
        }
        if (!isFullCommand) {
            if (NULL == (recv_arr = (char*)realloc(recv_arr, n+size))) {
            	perror("realloc");
            	free(recv_arr);
            	if (-1 == close(another_sockfd)) {
	            	perror("close");
	            }
	            pthread_exit(NULL);
            }
            strncpy(recv_arr+size, buf, n);
            size+=n;
        } else {
            if (NULL == (recv_arr = (char*)realloc(recv_arr, i+1+size))) {
            	perror("realloc");
            	free(recv_arr);
            	if (-1 == close(another_sockfd)) {
	            	perror("close");
	            }
	            pthread_exit(NULL);
            }
            strncpy(recv_arr+size, buf, i+1);
            break;
        }
    }
    char* pch;
    pch = strtok(recv_arr, " \r\n");
    if (NULL == pch) {
    	if (-1 == close(another_sockfd)) {
	        perror("close");
	    }
	    pthread_exit(NULL);
    }
    if (strncmp(pch, "NETWORK", 7)) {
        pthread_exit(NULL);
    }

    pch = strtok(NULL, " \r\n");
    if (NULL == pch) {
    	if (-1 == close(another_sockfd)) {
	        perror("close");
	    }
	    pthread_exit(NULL);
    }
    char* ptr = NULL;
    int count_of_servers = strtol(pch, &ptr, 10);
	if (ptr == pch || *ptr != 0) {
		fprintf(stderr, "Bad count servers format\n");
		pthread_exit(NULL);
	}

    Server_address serv_and_addr;

    for (; count_of_servers != 0; --count_of_servers) {
        pch = strtok(NULL, " \r\n");
        if (NULL == pch) {
	    	if (-1 == close(another_sockfd)) {
		        perror("close");
		    }
		    pthread_exit(NULL);	
	    }
     
        if (inet_pton(AF_INET, pch, &(serv_and_addr.sin_addr)) < 0) {
            if (0 != errno) {
                perror("inet_pton");
	            if (-1 == close(another_sockfd)) {
		        	perror("close");
		        }
                pthread_exit(NULL);
            } else {
                fprintf(stderr, "Bad format ip\n");
                if (-1 == close(another_sockfd)) {
		        	perror("close");
		        }
                pthread_exit(NULL);
            }
        }
        pch = strtok(NULL, " \r\n");
        if (NULL == pch) {
	    	if (-1 == close(another_sockfd)) {
		        perror("close");
		    }
		    pthread_exit(NULL);	
	    }
        ptr = NULL;
		serv_and_addr.sin_port = strtol(pch, &ptr, 10);
		if (ptr == pch || *ptr != 0) {
			fprintf(stderr, "Bad port format\n");
			pthread_exit(NULL);
		}
        pthread_mutex_lock(&connecting_servers_mutex);
        if (connecting_servers.find(serv_and_addr) == connecting_servers.end()) {
            connecting_servers.insert(std::pair<Server_address, bool>(serv_and_addr, 0));
        }
        pthread_mutex_unlock(&connecting_servers_mutex);
    }
    while (NULL != (pch = strtok(NULL, " \r\n"))) {
    	auto it = files_table.find(conn_server);
	    if (it == files_table.end() || it->second != string(pch)) {
	        files_table.insert(std::pair<Server_address, string>(conn_server, string(pch)));
	    }
	    pthread_mutex_unlock(&table_mutex);
    }
    free(recv_arr);
    pthread_t working_threads[count_of_servers];
    int numb = 0;
    pthread_mutex_lock(&connecting_servers_mutex);
    for (auto &connecting_server : connecting_servers) {
        if (!connecting_server.second) {
            Info_for_connect* new_server_info = (Info_for_connect*)calloc(1, sizeof(Info_for_connect));
            new_server_info->server_address.sin_addr.s_addr = connecting_server.first.sin_addr.s_addr;
            new_server_info->server_address.sin_port = ntohs(connecting_server.first.sin_port);
            new_server_info->my_addr = info_for_connect.my_addr;
            int err_code;
            if (0 != (err_code = pthread_create(working_threads+numb, NULL, connect_with_server, new_server_info))) {
                errno = err_code;
                perror("pthread_create");
        	numb++;
            }
        }
    }
    pthread_mutex_unlock(&connecting_servers_mutex);

    char ch;
    read(another_sockfd, &ch, 1);

	pthread_mutex_lock(&table_mutex);
    files_table.erase(conn_server);
    pthread_mutex_unlock(&table_mutex);

	pthread_mutex_lock(&connecting_servers_mutex);
    connecting_servers.erase(conn_server);
    pthread_mutex_unlock(&connecting_servers_mutex);    

    for (int i = 0; i < numb; ++i) {
    	pthread_join(working_threads[i], NULL);
    }

    if (-1 == close(another_sockfd)) {
        perror("close");
    }

    pthread_exit(NULL);
}

int main(int argc, char const *argv[]) {
    int sockfd, *clifd;
    char str[16];
    socklen_t len;
    if (-1 == (sockfd = socket(AF_INET, SOCK_STREAM, 0))) {
        perror("sockfd");
        exit(1);
    }

    struct sockaddr_in my_addr, cliaddr;
    bzero(&my_addr, sizeof(my_addr));
    my_addr.sin_family = AF_INET;

    if (argc < 2 || 3 == argc) {
        my_addr.sin_addr.s_addr = htonl(INADDR_ANY);
    } else {
        if (inet_pton(AF_INET, argv[1], &(my_addr.sin_addr)) == 0) {
            my_addr.sin_addr.s_addr = htonl(INADDR_ANY);
        }
    }

    if (-1 == bind(sockfd, (struct sockaddr*)&my_addr, sizeof(my_addr))) {
        perror("bind");
        exit(1);
    }

    len = sizeof(my_addr);
    if (-1 == getsockname(sockfd, (struct sockaddr*)&my_addr, &len)) {
        perror("getsockname");
        exit(1);
    }
    printf("local IP %s, local port %d\n",
           inet_ntop(AF_INET, &my_addr.sin_addr, str, sizeof(str)),
           ntohs(my_addr.sin_port));

    if (-1 == listen(sockfd, SOMAXCONN)) {
        perror("listen");
        exit(1);
    }

    pthread_t working_thread;
    int err_code;

    if (0 != (err_code = pthread_mutex_init(&table_mutex, NULL))) {
        errno = err_code;
        perror("pthread_mutex_init table_mutex");
        exit(1);
    }

    if (0 != (err_code = pthread_mutex_init(&connecting_servers_mutex, NULL))) {
        errno = err_code;
        perror("pthread_mutex_init connecting_servers_mutex");
        exit(1);
    }

    DIR* p_dir;
    struct dirent* p_dirent;
    p_dir = opendir("./");
    if (NULL == p_dir) {
        perror("opendir");
        exit(1);
    }

    Server_address key;
    key.sin_addr.s_addr = my_addr.sin_addr.s_addr;
    key.sin_port = ntohs(my_addr.sin_port);

    while (NULL != (p_dirent = readdir(p_dir))) {
        if (strncmp(p_dirent->d_name, ".\0", 2) && strncmp(p_dirent->d_name, "..\0", 3) && strncmp(p_dirent->d_name, "server\0", 7)) {
            pthread_mutex_lock(&table_mutex);
            files_table.insert(std::pair<Server_address, string>(key, string(p_dirent->d_name)));
            pthread_mutex_unlock(&table_mutex);
        }
    }

    if (argc >= 3) {
        int m = 0;
        if (argc == 3) m = 1;
        if (argc >= 4) m = 2;

        bool isSingle = true;
        struct Info_for_connect* info_for_connect = (struct Info_for_connect*)calloc(1, sizeof(Info_for_connect));
        if (inet_pton(AF_INET, argv[m], &(info_for_connect->server_address.sin_addr)) == 1) {
            isSingle = false;
        }
        info_for_connect->server_address.sin_port = htons(atoi(argv[m+1]));
        info_for_connect->my_addr = my_addr;
        if (!isSingle) {
            if (0 != (err_code = pthread_create(&working_thread, NULL, connect_with_server, info_for_connect))) {
                errno = err_code;
                perror("pthread_create");
                exit(1);
            }
        }

    }

    while(1) {
        clifd = (int*)calloc(1, sizeof(int));
        len = sizeof(cliaddr);
        if (-1 == (*clifd = accept(sockfd, (struct sockaddr*)&cliaddr, &len))) {
            perror("accept");
            exit(1);
        }

        int err_code;
        int count_pth_crt = 0;
        while (1) {
            if (0 != (err_code = pthread_create(&working_thread, NULL, do_work, clifd))) {
                if (err_code == EAGAIN) {
                    if (count_pth_crt == 6)  {
                        if (-1 == close(*clifd)) {
                            perror("close");
                            exit(1);
                        }
                        break;
                    }
                    sleep(5000);
                    ++count_pth_crt;
                } else {
                    errno = err_code;
                    perror("pthread_create");
                    exit(1);
                }
            } else {
                break;
            }
        }

    }

    if (-1 == close(sockfd)) {
        perror("close");
        exit(1);
    }

    return 0;
}
