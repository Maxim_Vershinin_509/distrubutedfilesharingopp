#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <netdb.h>
#include <string.h>
#include <fcntl.h>
#include <algorithm>

using namespace std;

ssize_t write_n_bytes(int descr, const char *buf, size_t n) {
	ssize_t n_was_written;
	size_t n_count = n;
	while (n_count > 0) {
		if ((n_was_written = write(descr, buf, n_count)) <= 0) {
			if (-1 == n_was_written) {
				return -1;
			} 
		}
		n_count -= n_was_written;
		buf += n_was_written;
	}
	return n;
}

ssize_t read_n_bytes(int descr, char *buf, size_t n) {
	ssize_t n_was_readen;
	size_t n_count = n;
	while (n_count > 0) {
		if ((n_was_readen = read(descr, buf, n_count)) <= 0) {
			if (-1 == n_was_readen) {
				return -1;
			} else if (0 == n_was_readen) {
				break;
			}
		}
		n_count -= n_was_readen;
		buf += n_was_readen;
	}
	return n - n_count;
}

void get_file_name(const char* mes, char* file_name, size_t len) {
	strncpy(file_name, mes, len);
	file_name[len] = '\0';
}

void answer_GET_command(int sockfd, const char* send, size_t len) {
	char file_name[len-3];
	get_file_name(send, file_name, len-4);
	int file_descr;
	if (-1 == (file_descr = open(file_name, O_CREAT | O_WRONLY, 0640))) {
		perror("open");
		exit(1);
	}

	char recv_arr[BUFSIZ];
	ssize_t n;
	fd_set rset;
	FD_ZERO(&rset);
	while (1) {
		FD_SET(STDIN_FILENO, &rset);
		FD_SET(sockfd, &rset);
		if (-1 == select(max(sockfd, STDIN_FILENO)+1, &rset, NULL, NULL, NULL)) {
			perror("select");
			exit(1);
		}
		if (FD_ISSET(sockfd, &rset)) {
			n = recv(sockfd, &recv_arr, BUFSIZ, 0);
			if (0 == n) break;
			if (-1 == write_n_bytes(file_descr, recv_arr, n)) {
				perror(file_name);
				exit(1);
			}
		} else if (FD_ISSET(STDIN_FILENO, &rset)) break;
	}
	if (-1 == n) {
		perror("recv");
		exit(1);
	}
	if (-1 == close(file_descr)) {
		perror("close");
		exit(1);
	}
}

void answer_GET_SIZE_command(int sockfd) {
	char buf[BUFSIZ];
	ssize_t n;
	bool isAnswer = false;
	while (1) {
		if ((n = recv(sockfd, buf, BUFSIZ, 0)) <= 0) {
			return;
		}
		int i;
		for (i = 1; i < n; ++i) {
			if ('\r' == buf[i-1] && '\n' == buf[i]) {
				isAnswer = true;
				break;
			}
		}
		if (!isAnswer) {
			if (-1 == write_n_bytes(STDOUT_FILENO, buf, n)) {
				perror("write_n_bytes");
				exit(1);
			}
		} else {
			if (-1 == write_n_bytes(STDOUT_FILENO, buf, i+1)) {
				perror("write_n_bytes");
				exit(1);
			}
			break;
		}
	}
}

void answer_SEARCH_command(int* sockfd, struct sockaddr_in* servaddr) {
	char buf[BUFSIZ];
	ssize_t n;
	bool isAnswer = false;
	while (1) {
		if ((n = recv(*sockfd, buf, BUFSIZ, 0)) <= 0) {
			return;
		}
		int i;
		for (i = 1; i < n; ++i) {
			if ('\r' == buf[i-1] && '\n' == buf[i]) {
				isAnswer = true;
				break;
			}
		}
		if (!isAnswer) {
			if (-1 == write_n_bytes(STDOUT_FILENO, buf, n)) {
				perror("write_n_bytes");
				exit(1);
			}
		} else {
			if (-1 == write_n_bytes(STDOUT_FILENO, buf, i+1)) {
				perror("write_n_bytes");
				exit(1);
			}
			break;
		}
	}
	char* pch = strtok(buf, " \r\n");
	if (NULL == pch) return;
	pch = strtok(NULL, " \r\n");
	if (NULL == pch) return;
	struct sockaddr_in another_serv;
	bzero(&another_serv, sizeof(another_serv));
	another_serv.sin_family = AF_INET;
	if (inet_pton(AF_INET, pch, &another_serv.sin_addr) <= 0) {
		return;
	}
	pch = strtok(NULL, " \r\n");
	if (NULL == pch) return;
	char *ptr = NULL;
	another_serv.sin_port = htons(strtol(pch, &ptr, 10));
	if (ptr == pch || *ptr != 0) {
		return;
	}
	if (another_serv.sin_port != servaddr->sin_port || another_serv.sin_addr.s_addr != servaddr->sin_addr.s_addr) {
		if (-1 == close(*sockfd)) {
			perror("close");
			return;
		}
		servaddr->sin_port = another_serv.sin_port;
		servaddr->sin_addr.s_addr = another_serv.sin_addr.s_addr;
		if (-1 == (*sockfd = socket(AF_INET, SOCK_STREAM, 0))) {
			perror("socket");
			exit(1);
		}
		if (-1 == connect(*sockfd, (struct sockaddr*)&another_serv, sizeof(another_serv))) {
			perror("connect");
			exit(1);
		}
		printf("Connection to another server\n");
	}
}

int main(int argc, char const *argv[]) {
	int sockfd;
	struct sockaddr_in servaddr;

	if (argc < 3) {
		fprintf(stderr, "Bad main arguments, no IP\n");
		exit(0);
	}

	if (-1 == (sockfd = socket(AF_INET, SOCK_STREAM, 0))) {
		perror("socket");
		exit(1);
	}

	bzero(&servaddr, sizeof(servaddr));

	servaddr.sin_family = AF_INET;
	servaddr.sin_port = htons(atoi(argv[2]));
	if (-1 == inet_pton(AF_INET, argv[1], &servaddr.sin_addr)) {
		perror("inet_pton");
		exit(1);
	}

	if (-1 == connect(sockfd, (struct sockaddr*)&servaddr, sizeof(servaddr))) {
		perror("connect");
		exit(1);
	}

	char send_arr[BUFSIZ];
	while (NULL != fgets(send_arr, BUFSIZ-3, stdin)) {
		size_t len = strlen(send_arr)-1;
		send_arr[len] = '\r';
		send_arr[len+1] = '\n';
		int err_code;
		if (-1 == (err_code = write_n_bytes(sockfd, send_arr, len+2))) {
			perror("write_n_bytes");
			exit(1);
		}
		if (0 == err_code) break;

		if (!strncmp(send_arr, "GET ", 4)) {
			answer_GET_command(sockfd, send_arr+4, len);
		} else if (!strncmp(send_arr, "GET_SIZE ", 9)) {
			answer_GET_SIZE_command(sockfd);
		} else if (!strncmp(send_arr, "SEARCH ", 7)) {
			answer_SEARCH_command(&sockfd, &servaddr);
		}
	}

	if (errno != 0) {
		perror("fgets");
		exit(1);
	}

	if (-1 == close(sockfd)) {
		perror("close");
		exit(1);
	}

	return 0;
}